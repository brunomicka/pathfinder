'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpCopy = require('gulp-copy');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var fs = require("fs");
var rename = require("gulp-rename");
var path = require('path');
var browserSync = require('browser-sync').create();

/////////////////////
//// SASS processing

gulp.task('sass', function () {
 return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss','./app/assets/sass/**/*.scss'])
   .pipe(sass().on('error', sass.logError))
   .pipe(gulp.dest('./dist/assets/css'));
});

gulp.task('sass:prod', function () {
 return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss','./app/assets/sass/**/*.scss'])
   .pipe(sass().on('error', sass.logError))
   .pipe(gulp.dest('./dist/assets/css'))
   .pipe(sourcemaps.init())
   .pipe(cleanCSS())
   .pipe(sourcemaps.write())
   .pipe(rename('style.min.css'))
   .pipe(gulp.dest('./dist/assets/css'));
});

gulp.task('sass:watch', function () {
 gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss','./app/assets/sass/**/*.scss'], ['sass']);
});



//Move JS Files to src/js

gulp.task('js', function(){
  return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js','node_modules/jquery/dist/jquery.min.js',
              'node_modules/popper.js/dist/umd/popper.min.js'])
              .pipe(gulp.dest('./dist/assets/js'))
              .pipe(browserSync.stream());
});


/////////////////////
//// Html templating

const templates = [
  {tag: "<headertemplate/>", path: "./app/templates/header.html"},
  {tag: "<headerimports/>", path: "./app/templates/header-imports.html"},
  {tag: "<headerbar/>", path:"./app/templates/header-bar.html"},
  {tag: "<footertemplate/>", path: "./app/templates/footer.html"},
  {tag: "<footerimports/>", path: "./app/templates/footer-imports.html"},
  {tag: "<menuestendido/>", path:"./app/templates/menu.html"}
];

gulp.task('processhtml', function(){
  var glob = require('glob');
  var htmlFiles = glob.sync('./app/*.html');

  if(!htmlFiles || htmlFiles.length == 0)
    return;

  for(let f = 0; f < htmlFiles.length; f++) {
    let fileName = htmlFiles[f].split('/').pop();
    let fileContent = fs.readFileSync(htmlFiles[f], "utf8");

    templates.forEach(template => {
      if(fileContent.indexOf(template.tag) > -1) {
        let templateContent = fs.readFileSync(template.path, "utf8");
        fileContent = fileContent.replace(template.tag, templateContent);
      }
    });

    _mkdirSync('./dist/')
    fs.writeFileSync('./dist/' + fileName, fileContent);
  }
});

gulp.task('processhtml:watch', function(){
  gulp.watch('./app/**/*.html', ['processhtml']);
});

function _mkdirSync(dir){
  if (fs.existsSync(dir)){
      return
  }

  try{
      fs.mkdirSync(dir)
  }catch(err){
      if(err.code == 'ENOENT'){
          myMkdirSync(path.dirname(dir)) //create parent dir
          myMkdirSync(dir) //create dir
      }
  }
}

/////////////////////
//// Copying assets

const assetsFolders = [ './app/assets/js/**/*.*', './app/assets/images/**/*.*', './app/assets/icon/**/*.*', './app/assets/fonts/**/*.*' ];

gulp.task('copyassets', function () {
  return assetsFolders.forEach(folder => {
    var folderPath = folder.replace('./app/assets/', '');
    folderPath = folderPath.replace('/**/*.*', '');

    gulp.src(folder)
      .pipe(gulp.dest('./dist/assets/' + folderPath));
  });
 });
 
 gulp.task('copyassets:watch', function () {
  gulp.watch(assetsFolders, ['copyassets']);
 });

/////////////////////
//// Brwoser sync server

gulp.task('serve', function() {

    browserSync.init({
        server: "./dist"
    });

    gulp.watch("dist/**/*.*").on('change', browserSync.reload);
});


///////

gulp.task('default', ['sass','js']);

gulp.task('build', ['sass:prod', 'copyassets', 'processhtml']);

gulp.task('build:dev', ['sass', 'copyassets', 'processhtml']);

gulp.task('dev', ['sass:watch', 'copyassets:watch', 'processhtml:watch', 'serve']);