$(function(){
    $('input[type="text"],input[type="password"],input[type="email"]').keyup(function(ev, el) {
        setTimeout(function() {
            var text = $(ev.currentTarget).val();
            console.log(text.length);
            if(text.length == 0 && !$(ev.currentTarget).is(":focus")) {
                $(ev.currentTarget).removeClass('tem-valor');
            }
            else {
                $(ev.currentTarget).addClass('tem-valor');
            }
        }, 80);
    });

    $('input[type="text"],input[type="password"],input[type="email"]').focus(function(ev, el) {
        
        if(!$(ev.currentTarget).hasClass('tem-valor')){
            $(ev.currentTarget).addClass('tem-valor');
        }
    });


    $('input[type="text"],input[type="password"],input[type="email"]').blur(function(ev, el) {
        var text = $(ev.currentTarget).val();
        if(text.length == 0){
            $(ev.currentTarget).removeClass('tem-valor');
        }
    });
});


function checkAll(){
    $(".table-users tbody tr th input").each(function(){
        $(this).prop("checked", document.getElementById("chkAll").checked);
    });
}

function tableDelete(){
    $(".alert-selected-row").hide();
    $(".table-users tbody tr th input").each(function(){
      $(this).prop("checked",false);
    });
}

var menus = {
    Search: 1,
    User: 2,
    Profile : 3,
    Permission: 4
};

function setMenuActive(enumMenu){
    $(".main-menu ul li a[data-menu="+ enumMenu +"]").parent().addClass("active");
}

function stripeTables() {
    $("tbody tr").removeClass('odd-row');
    $("tbody tr:not(.hidden):odd").addClass('odd-row');
}